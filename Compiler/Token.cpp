#include "Token.h"

//Just a representation of the TOKEN enum as strings
static const string tokens[] = { "NULL", "IDENTIFIER", "INTEGER", "DOUBLE", "EQ", "EQEQ", "OPEN_PAREN", "CLOSE_PAREN", "OPEN_BRACE", "CLOSE_BRACE", "PLUS", "MINUS", "TIMES",
"DIVIDE", "MODULO", "IF", "ELSE", "WHILE", "FOR", "SEMICOLON", "ERROR", "WHITESPACE", "EOF" };

const string* Token::tokStrings = tokens;

/*
* Create a default null token
*/
Token::Token()
{
	this->token = TOKEN::NULL_TOKEN;
	this->type = "null";
	this->value = "null";
	this->pos = std::make_pair(-1, -1);
}

/*
* Create a token with the specified type, value and position
*/
Token::Token(TOKEN token, string type, string value, std::pair<int, int> p)
{
	this->token = token;
	this->type = type;
	this->value = value;
	this->pos = p;
}

/*
* Print out the token info
*/
void Token::print()
{
	std::cout << this->type << " token of type " << Token::tokStrings[(int)this->token] << " with value " << this->value;
}