#include<chrono>
#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <memory>
//#include "State.h"
//#include "Token.h"
//#include "Lexer.h"
#include "Parser.h"
#include <windows.h>

using std::string;
using std::map;
using std::unordered_map;
using std::ifstream;
using std::unique_ptr;
using std::cout;
using std::tr1::unordered_set;




int main()
{
	//auto begin = std::chrono::high_resolution_clock::now();
	//Create a simple lexer to tokenize a string with integers and identifiers
	TransitionRange alphaLower('a', 'z', StateType::IDENTIFIER_STATE);
	TransitionRange alphaUpper('A', 'Z', StateType::IDENTIFIER_STATE);
	TransitionRange digits('0', '9', StateType::INTEGER_STATE);//for integer state
	TransitionRange idDigits('0', '9', StateType::IDENTIFIER_STATE); //for identifier state
	TransitionRange doubleDigits('0', '9', StateType::DOUBLE_STATE); //for double state

	Lexer scanner;
	scanner.readFile("input.txt");
	
	//Add the states that the lexer can be in, corresponding to the possible tokens
	scanner.addState(StateType::START_STATE, StateType::ERROR_STATE);
	scanner.getState(StateType::START_STATE)->addRange(alphaLower);
	scanner.getState(StateType::START_STATE)->addRange(alphaUpper);
	scanner.getState(StateType::START_STATE)->addRange(digits);
	scanner.getState(StateType::START_STATE)->addTransitions(" \n\r\t", StateType::WHITESPACE_STATE);
	scanner.getState(StateType::START_STATE)->addTransitions("=(){}+-/*%;", StateType::SYMBOL_STATE);
	scanner.getState(StateType::START_STATE)->addTransition('_', StateType::IDENTIFIER_STATE);
	scanner.getState(StateType::START_STATE)->addTransition('\0', StateType::EOF_STATE);

	scanner.addState(StateType::SYMBOL_STATE);
	scanner.getState(StateType::SYMBOL_STATE)->addTransitions("=(){}+-/*%;", StateType::SYMBOL_STATE);

	scanner.addState(StateType::INTEGER_STATE);
	scanner.getState(StateType::INTEGER_STATE)->addRange(digits);
	
	scanner.addState(StateType::DOUBLE_STATE);
	scanner.getState(StateType::INTEGER_STATE)->addTransition('.', StateType::DOUBLE_STATE);
	scanner.getState(StateType::DOUBLE_STATE)->addRange(doubleDigits);

	scanner.addState(StateType::IDENTIFIER_STATE);
	scanner.getState(StateType::IDENTIFIER_STATE)->addRange(alphaLower);
	scanner.getState(StateType::IDENTIFIER_STATE)->addRange(alphaUpper);
	scanner.getState(StateType::IDENTIFIER_STATE)->addRange(idDigits);
	scanner.getState(StateType::IDENTIFIER_STATE)->addTransition('_', StateType::IDENTIFIER_STATE);

	scanner.addState(StateType::WHITESPACE_STATE);
	scanner.getState(StateType::WHITESPACE_STATE)->addTransitions(" \r\n\t", StateType::WHITESPACE_STATE);

	scanner.addState(StateType::END_OF_TOKEN_STATE, StateType::END_OF_TOKEN_STATE);

	scanner.addState(StateType::EOF_STATE, StateType::EOF_STATE);

	scanner.addState(StateType::ERROR_STATE, StateType::ERROR_STATE);
	
	//Time how long it takes to parse the input
	auto begin = std::chrono::high_resolution_clock::now();

	//Parse and evaluate the expression provided in input.txt
	Parser parser(scanner);
	try
	{
		Value result = parser.evalExp();
		//If you want to print the tree instead of evaluate it. One or the other
		//parser.printExp();
		if (result.Type == Value::DOUBLE)
			cout << result.d;
		else
			cout << result.i;

		cout << "\n";
	}
	catch (std::exception& e)
	{
		cout << e.what() << "\n";
	}

	
	//This prints out the Tokens that were parsed instead of parsing and evaluating
	/*Token result = parser.parseExp();

	Token token;
	do
	{
		
		token = scanner.getToken();
		token.print();
		cout << "\n";
	} while (scanner.hasValidNextToken());*/

	//Print the time lapse in milliseconds
	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> diff = std::chrono::duration_cast<std::chrono::duration<double>>(end - begin);
	
	cout << diff.count()*1000.0 << " milliseconds\n";

	//Wait so that the console stays open until input is entered
	string wait;
	std::cin >> wait;
	return 0;
}