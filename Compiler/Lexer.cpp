#include "Lexer.h"

/*
* Associate the strings with their corresponding tokens and set the initial token lexeme
* to empty
*/
Lexer::Lexer() : pos(-1), lineNumber(1)
{
	//Associate string literals with their corresponding TOKEN
	keywords["if"] = TOKEN::IF_TOKEN;
	keywords["else"] = TOKEN::ELSE_TOKEN;
	keywords["while"] = TOKEN::WHILE_TOKEN;
	keywords["for"] = TOKEN::FOR_TOKEN;

	symbols["="] = TOKEN::EQ_TOKEN;
	symbols["=="] = TOKEN::EQEQ_TOKEN;
	symbols["("] = TOKEN::OPEN_PAREN_TOKEN;
	symbols[")"] = TOKEN::CLOSE_PAREN_TOKEN;
	symbols["{"] = TOKEN::OPEN_BRACE_TOKEN;
	symbols["}"] = TOKEN::CLOSE_BRACE_TOKEN;
	symbols["+"] = TOKEN::PLUS_TOKEN;
	symbols["-"] = TOKEN::MINUS_TOKEN;
	symbols["*"] = TOKEN::TIMES_TOKEN;
	symbols["/"] = TOKEN::DIVIDE_TOKEN;
	symbols["%"] = TOKEN::MODULO_TOKEN;
	symbols[";"] = TOKEN::SEMICOLON_TOKEN;

	//Start with an empty token lexeme
	lexeme = "";
}

Lexer::~Lexer()
{

}

/*
* Read the input file directly into a string that is used by the lexer.
*/
bool Lexer::readFile(string path)
{
	bool successful = true;
	buffer = "";
	std::ifstream fin(path, std::ios::in | std::ios::binary);
	//If the file opened, resize the input buffer to the file size and read the entire file into it
	if (fin)
	{
		fin.seekg(0, std::ios::end);
		buffer.resize(fin.tellg());
		fin.seekg(0, std::ios::beg);

		fin.read(&buffer[0], buffer.size());
		buffer += '\0';

		successful = true;
	}
	else
	{
		successful = false;
	}

	fin.close();

	//If the read was successful, read the first char
	if (successful && buffer.length() > 0)
	{
		readInput(input);
	}

	return successful;
}

/*
* Add a state to the lexer (e.g. INTEGER, IDENTIFIER)
*/
void Lexer::addState(StateType type, StateType fail)
{

	this->states[type] = std::make_unique<State>(type, fail);
}

/*
* Read the next character from the input string and increment the index.
* @return bool True if the character is successfully read, false otherwise.
*/
bool Lexer::readInput(char &input)
{
	pos++;
	linePos++;
	if (pos < buffer.length())
		input = buffer[pos];

	if (input == '\n')
	{
		lineNumber++;
		linePos = 0;
	}

	return input != '\0' && pos < buffer.length();
}

/*
* Get the next token lexeme string
*/
string Lexer::readTokenString()
{
	currentState = StateType::START_STATE;
	nextState = StateType::START_STATE;

	string token = "";


	//Continue to follow the State transitions until the Error state is reached.
	while (nextState != StateType::ERROR_STATE && nextState != StateType::END_OF_TOKEN_STATE && nextState != StateType::EOF_STATE)
	{
		//Get the next State based on the current State and input char
		nextState = states[currentState]->getTransition(input);
		
		unordered_map<string, TOKEN>::iterator it;

		switch (nextState)
		{
		case StateType::START_STATE:
		case StateType::INTEGER_STATE:
		case StateType::DOUBLE_STATE:
		case StateType::IDENTIFIER_STATE:
		case StateType::WHITESPACE_STATE:
		case StateType::ERROR_STATE:
			//Append the input char to the token value and update the current state. Then read the next input char.
			token += input;
			currentState = nextState;
			if (!readInput(input))
			{
				//End of file/buffer
			}
			break;
		case StateType::SYMBOL_STATE:
			//Append the input char to the token value and update the current state. Then read the next input char.
			token += input;
			currentState = nextState;
			
			if (!readInput(input))
			{
				//End of file/buffer
			}
			break;
		case StateType::END_OF_TOKEN_STATE:
			//No more input or no transition to make. Do nothing.
			break;
		case StateType::EOF_STATE:
			//The end of file was reached. Can only be reached from StartState
			currentState = nextState;
			break;
		}
	}
	//If the token contains symbol(s), add them to the symbol queue so that they are matched by longest first
	if (currentState == StateType::SYMBOL_STATE)
	{
		this->processSymbolQueue(token);
	}


	return token;
}

/*
* Convert the current State and lexeme into a Token and return it
*/
Token Lexer::getToken(string lexeme)
{
	Token token;
	unordered_map<string, TOKEN>::iterator it;
	switch (currentState)
	{
	case StateType::START_STATE:
	case StateType::ERROR_STATE:
		token.token = TOKEN::ERROR_TOKEN;
		token.type = "error";
		token.value = lexeme;
		token.pos = this->getPos();
		break;
	case StateType::INTEGER_STATE:
		token.token = TOKEN::INTEGER_TOKEN;
		token.type = "number";
		token.value = lexeme;
		token.pos = this->getPos();
		break;
	case StateType::DOUBLE_STATE:
		token.token = TOKEN::DOUBLE_TOKEN;
		token.type = "double";
		token.value = lexeme;
		token.pos = this->getPos();
		break;
	case StateType::IDENTIFIER_STATE:
		//keywords are a subset of identifiers, so check if the identifier is a keyword reserved by the language
		it = keywords.find(lexeme);
		if (it != keywords.end())
		{
			token.token = it->second;
			token.type = "keyword";
			token.value = lexeme;
			token.pos = this->getPos();
		}
		else
		{
			token.token = TOKEN::IDENTIFIER_TOKEN;
			token.type = "identifier";
			token.value = lexeme;
			token.pos = this->getPos();
		}
		break;
	case StateType::SYMBOL_STATE:
		//check if it's a valid symbol/operator, so check if the symbol is in the map of symbols
		it = symbols.find(lexeme);
		if (it != symbols.end())
		{
			token.token = it->second;
			token.type = "symbol";
			token.value = lexeme;
			token.pos = this->getPos();
		}
		else
		{
			token.token = TOKEN::ERROR_TOKEN;
			token.type = "error";
			token.value = lexeme;
			token.pos = this->getPos();
		}
		break;
	case StateType::WHITESPACE_STATE:
		token.token = TOKEN::WHITESPACE_TOKEN;
		token.type = "whitespace";
		token.value = lexeme;
		token.pos = this->getPos();
		break;
	case StateType::EOF_STATE:
		//The end of file was reached. Can only be reached from StartState
		token.token = TOKEN::EOF_TOKEN;
		token.type = "eof";
		token.value = EOF;
		token.pos = this->getPos();
		break;
	}

	return token;
}

/*
* Put all the necessary tokens onto the symbol queue. SHould be invoked after a call to readTokenString()
*/
bool Lexer::processSymbolQueue(string lex)
{
	bool symbolPushed = false;
	string original = lex;
	while (!lex.empty())
	{
		/*
		* Loop through lex, removing a char from the end each iteration, until empty or a symbol is found
		* That symbol will be put on the queue to be processed for the next token, and then the remaining lex string
		* will be processed 
		* For example: original = "*((" -> original = "((" -> original = "("
		*/
		auto it = symbols.find(lex);
		if (it != symbols.end())
		{
			this->symbolQueue.push(lex);
			lex = original.substr(lex.length());
			original = lex;

			symbolPushed = true;
		}
		else
		{
			lex.pop_back();
		}
	}
	return symbolPushed;
}

/*
* Read the token string and create a Token out of it
* @return string The next lexeme/token value from the input string
*/
Token Lexer::getToken()
{
	Token tok;
	if (this->symbolQueue.empty())
	{
		//If the symbolQueue is empty, read the next token from the input
		lexeme = readTokenString();

		//if there is no symbol in the queue to process, just set the next token
		if (this->symbolQueue.empty())
		{
			tok = getToken(lexeme);
		}
		else
		{
			//Otherwise set the token to the symbol
			string symbol = this->symbolQueue.front();
			this->symbolQueue.pop();

			//Set the token
			tok = getToken(symbol);
		}
	}
	else
	{
		//If the symbolQueue has a symbol, use it as the next token instead of reading a new token from the input
		string symbol = this->symbolQueue.front();
		this->symbolQueue.pop();

		tok = getToken(symbol);
	}
	
	return tok;
}

/*
* Get the input buffer containing the code
*/
string Lexer::getBuffer()
{
	return this->buffer;
}

/*
* Get the State object of the specified state
*/
std::unique_ptr<State>& Lexer::getState(StateType state)
{
	return this->states[state];
}

/*
* Make sure the next token is not EOF_STATE
*/
bool Lexer::hasValidNextToken()
{
	return this->nextState != StateType::EOF_STATE;
}

/*
* Get the line and column position of the beginning of the current token
* Returns a pair with first as line number, second as column
*/
std::pair<int, int> Lexer::getPos()
{
	
	return std::make_pair(this->lineNumber, this->linePos- this->lexeme.length());
}