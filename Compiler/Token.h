#ifndef TOKEN_H
#define TOKEN_H

#include <string>
#include <iostream>

using std::string;
using std::pair;

//Possible TOKENs that may be in the language, most are not or may not ever be implemented
enum class TOKEN {
	NULL_TOKEN, IDENTIFIER_TOKEN, INTEGER_TOKEN, DOUBLE_TOKEN, EQ_TOKEN, EQEQ_TOKEN, OPEN_PAREN_TOKEN, CLOSE_PAREN_TOKEN, OPEN_BRACE_TOKEN, CLOSE_BRACE_TOKEN, PLUS_TOKEN, 
	MINUS_TOKEN, TIMES_TOKEN, DIVIDE_TOKEN, MODULO_TOKEN, IF_TOKEN, ELSE_TOKEN, WHILE_TOKEN, FOR_TOKEN, SEMICOLON_TOKEN, ERROR_TOKEN, WHITESPACE_TOKEN, EOF_TOKEN };

class Token
{
public:
	static const string* tokStrings;
	TOKEN token;
	string type;
	string value;
	std::pair<int, int> pos;

	Token();

	Token(TOKEN token, string type, string value, std::pair<int, int> p);

	void print();
};

#endif