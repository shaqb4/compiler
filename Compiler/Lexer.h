#ifndef LEXER_H
#define LEXER_H

#include "State.h"
#include "Token.h"
#include <map>
#include <queue>
#include <memory>
#include <fstream>

using std::map;
using std::pair;

class Lexer
{
protected:
	
	StateType currentState, nextState;
	int pos, lineNumber, linePos;
	string buffer, lexeme;
	std::queue<string> symbolQueue;

	//Initialize the input character
	char input;

	map<StateType, std::unique_ptr<State> > states;
	unordered_map<string, TOKEN> keywords, symbols;

	bool processSymbolQueue(string lex);

public:

	Lexer();

	~Lexer();

	bool readFile(string path);

	void addState(StateType type, StateType fail = StateType::END_OF_TOKEN_STATE);

	/*
	* Read the next character from the input string and increment the index.
	* @return bool True if the character is successfully read, false otherwise.
	*/
	bool readInput(char &input);

	string readTokenString();

	Token getToken(string lexeme);

	/*
	* Read the token by reading the input one character at a time and following the State transitions.
	* @return string The next lexeme/token value from the input string
	*/
	Token getToken();

	string getBuffer();

	std::unique_ptr<State>& getState(StateType state);

	//StateType getNextState();

	bool hasValidNextToken();

	/*
	* @return the line and column location of the lexer in the input string
	*/
	std::pair<int, int> getPos();
};

#endif