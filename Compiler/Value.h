#ifndef VALUE_H
#define VALUE_H

#include <iostream>

/*
* A value to hold the result from evaluation of the AST with the evaluator visitor
* A tagged union, but could be replaced by the new std::variant type
*/
struct Value
{
	enum { DOUBLE, INTEGER, INVALID } Type;

	Value()
	{
		this->Type = Value::INVALID;
	}

	union
	{
		int i;
		double d;
	};

	void setValue(double d)
	{
		this->d = d;
		this->Type = Value::DOUBLE;
	}

	void setValue(int i)
	{
		this->i = i;
		this->Type = Value::INTEGER;
	}

	void print()
	{
		switch (this->Type)
		{
		case Value::DOUBLE:
			std::cout << this->d;
			break;
		case Value::INTEGER:
			std::cout << this->i;
			break;
		}

	}

	//getValue()
};

/*class Value
{
public:

	Value(ValueType type)
	{
		this->type = type;
	}

protected:
	ValueType type;
};

class IntValue : public Value
{
	IntValue(ValueType type) : Value(type) { }

	void setValue(int val);

	int getValue();

protected:
	int value;
};

class FloatValue : public Value
{
	FloatValue(ValueType type) : Value(type) { }

	void setValue(double val);

	double getValue();

protected:
	double value;
};*/

/*template <typename T>
class NumberValue : Value
{
public:

	NumberValue(ValueType type) : Value(type)
	{ }

	void setValue(T val)
	{
		if (this->type == ValueType::INVALID)
		{
			throw std::runtime_error("Tried to set a value of invalid type\n");
		}
		this->value = val;
	}

	T getValue()
	{
		if (this->type == ValueType::INVALID)
		{
			throw std::runtime_error("Tried to get an Invalid type\n");
		}
		return this->value;
	}



protected:
	//ValueType type;
	T value;
};*/
#endif