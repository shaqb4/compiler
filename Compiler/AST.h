#ifndef AST_H
#define AST_H

#include <vector>
#include <memory>
#include "Visitor.h"
#include "Token.h"

//Possible types
enum class DataType {INTEGER, DOUBLE, BOOL, STRING, UNSET};

/*
* The abstract base class that nodes in the abstract syntax tree will extend in order to traversed using the visitor pattern.
* CURRENT STATUS: Visitor just evaluates the mathematical expressions
* TODO: use smart pointers
*/
class Visitable
{
public:
	Visitable();

	virtual void accept(Visitor* v) = 0;
};

/*
* The base class for the abstract syntax tree nodes. Accepts a visitor and has methods for accessing child nodes
*/
class AST : public Visitable
{
public:
	virtual void accept(Visitor* v) = 0;

	virtual int numChildren() = 0;

	virtual AST* getChild(int i) = 0;
};

/*
* 
* CURRENT STATUS: Not implemented
*/
class Statement : public AST
{
public:
	Statement() {}
};

class Expression : public AST
{
public:
	Expression() {}

	DataType getType() { return this->type; }

protected:
	DataType type;
};

/*
* Base class for nodes representing a literal value such as 5, 5.6, or "str"
* It has no children
*/
class Literal : public Expression
{
public:
	Literal() {}

	int numChildren() { return 0; }

	AST* getChild(int i) { return nullptr; }
};

/*
* An integer literal that simply represents an int value such as 5
*/
class Integer : public Literal
{
public:
	Integer(int val) : value(val) { this->type = DataType::INTEGER; }

	void accept(Visitor* v)
	{
		v->visit(this);
	}

	int getValue() { return value; }

protected:
	int value;

};


/*
* A double literal that simply represents a double value such as 5.6
*/
class Double : public Literal
{
public:
	Double(double val) : value(val) { this->type = DataType::DOUBLE; }

	void accept(Visitor* v)
	{
		v->visit(this);
	}

	double getValue() { return value; }

protected:
	double value;

};

/*
* A base class for operators that takes two Expressions as operands, and therefore has 2 children nodes
* Has method to get the operator TOKEN
* TODO: Should use smart pointers correctly
*/
class BinOp : public Expression
{
public:
	BinOp(std::unique_ptr<Expression> lhs, TOKEN o, std::unique_ptr<Expression> rhs) : Expression(), op(o)
	{
		this->args[0] = std::move(lhs);
		this->args[1] = std::move(rhs);
	}

	int numChildren() { return 2; }
	Expression* getChild(int i) { return args[i].get(); }

	TOKEN getOper() { return this->op; }

protected:
	std::unique_ptr<Expression> args[2];
	TOKEN op;
};


/*
* A binary operator that represents arithmetic expressions and uses the +, -, *, / operators
*/
class ArithOp : public BinOp
{
public:
	ArithOp(std::unique_ptr<Expression> lhs, TOKEN o, std::unique_ptr<Expression> rhs) : BinOp(std::move(lhs), o, std::move(rhs)) {}
	
	void accept(Visitor* v)
	{
		v->visit(this);
	}
};

/*
*
* CURRENT STATUS: Not implemented
*/
class Identifier : public Statement
{
public:
	Identifier(std::string name) : ident(name) {}

protected:
	std::string ident;
};

/*
*
* CURRENT STATUS: Not implemented
*/
class StatementBlock : public Statement
{
public:
	StatementBlock(){}


protected:
	std::vector<Statement> statements;
};

#endif