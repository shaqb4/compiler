#include "Parser.h"
#include "AST.h"
#include <sstream>

Parser::Parser(Lexer& lex) : scanner(lex)
{
	//The precedence of each operator, the higher number means higher precendence
	precedence["+"] = 20;
	precedence["-"] = 20;
	precedence["*"] = 30;
	precedence["/"] = 30;
	precedence["%"] = 40;

	this->getNextToken();
}

/*
* Get the next non-whitespace token
*/
Token Parser::getNextToken()
{
	this->current = this->scanner.getToken();
	if (this->current.token == TOKEN::WHITESPACE_TOKEN)
	{
		this->current = this->scanner.getToken();
	}
	else if (this->current.token == TOKEN::ERROR_TOKEN)
	{
		std::stringstream err;
		err << "Error encountered at line " << this->current.pos.first << ":" << this->current.pos.second;
		throw std::runtime_error(err.str().c_str());
	}
	return this->current;
}

/*
* Get the next token if the current one matches tok
*/
bool Parser::accept(TOKEN tok) 
{
	if (this->current.token == tok)
	{
		this->getNextToken();
		return true;
	}
	return false;
}

/*
* Accept tok and give an error if does not match
*/
bool Parser::expect(TOKEN tok)
{
	if (this->accept(tok))
	{
		return true;
	}
	std::cerr << "Expect: expected token type " << Token::tokStrings[static_cast<int>(tok)] << " , got " << Token::tokStrings[static_cast<int>(this->current.token)] << "\n";
	return false;
}

/*
* Whether the current token matches tok
*/
bool Parser::currentIs(TOKEN tok)
{
	return (this->current.token == tok);	
}

/*
* Parse primary expressions, currently only int and double literals
*/
std::unique_ptr<Expression> Parser::parsePrimaryExp()
{
	switch (this->current.token)
	{
		case TOKEN::INTEGER_TOKEN:
		{
			return this->parseIntegerExp();
		}
		case TOKEN::DOUBLE_TOKEN:
		{
			return this->parseDoubleExp();
		}
		default:
		{
			return nullptr;
		}
	}
}

/*
* Convert the current token into an Integer Expression node for the AST and return it
*/
std::unique_ptr<Expression> Parser::parseIntegerExp()
{
	if (this->currentIs(TOKEN::INTEGER_TOKEN))
	{
		int val = std::stoi(this->current.value);
		auto result = std::make_unique<Integer>(val);
		this->getNextToken();

		return std::move(result);
	}
	return nullptr;
}

/*
* Convert the current token into a Double Expression node for the AST and return it
*/
std::unique_ptr<Expression> Parser::parseDoubleExp()
{
	if (this->currentIs(TOKEN::DOUBLE_TOKEN))
	{
		double val = std::stod(this->current.value);
		auto result = std::make_unique<Double>(val);
		this->getNextToken();

		return std::move(result);
	}
	return nullptr;
}

/*
* Return the operator precedence of the current token, or -1 if it is not a valid operator
*/
int Parser::getTokenPrecedence()
{
	if (!isascii(this->current.value[0]))
	{
		return -1;
	}

	auto it = this->precedence.find(this->current.value);
	if (it == this->precedence.end())
	{
		return -1;
	}
	int tokPrec = this->precedence[this->current.value];
	if (tokPrec < 0)
	{
		return -1;
	}
	return tokPrec;
}


/*
* Use the operator precedence values and the left-hand side Expression to create and return an ArithOp AST node
*/
std::unique_ptr<Expression> Parser::parseBinOpExpr(std::unique_ptr<Expression> lhs, int minPrec)
{
	while (true)
	{
		int tokPrec = getTokenPrecedence();

		//If current precedence is less than the min, than just return the left-hand side.
		if (tokPrec < minPrec)
		{
			return lhs;
		}
		//Get the current operator and next token
		Token binOp = this->current;
		this->getNextToken();

		std::unique_ptr<Expression> rhs;
		//If '(' is encountered, a new Expression needs to be parsed, otherwise parse the primary Expression
		if (this->currentIs(TOKEN::OPEN_PAREN_TOKEN))
		{
			//Pass the current token precedence+1 so that it is treated as a rhs expression 
			//e.g if input is 5*(6) + 3, parse don't parse (6) + 3 as the rhs because + has a lower precedence than *
			rhs = this->parseBinOpExp(tokPrec+1);
		}
		else
		{
			rhs = this->parsePrimaryExp();
		}
		
		if (!rhs)
		{
			return nullptr;
		}

		//Get the precedence of the next operator. E.g. for 6+5*6, the precedence of '*' because 6+5 has already been parsed
		int nextPrec = getTokenPrecedence();

		//Make sure current token is an operator and has more precedence than the initial operator E.g. '+'
		//Since '*' has higher precedence, the whole expression of 5*6 must be parse as the rhs
		if (this->current.type == "symbol" && tokPrec < nextPrec)
		{
			//Recursively parse the right-hand side Expression
			rhs = this->parseBinOpExpr(std::move(rhs), tokPrec + 1);
			if (!rhs)
			{
				return nullptr;
			}
		}

		//Return the ArithOp Expression node for the AST
		lhs = std::make_unique<ArithOp>(std::move(lhs), binOp.token, std::move(rhs));
	}
}

/*
* Parse a binary operator expression, with or without parentheses. 
* minPrec defaults to 0
*/
std::unique_ptr<Expression> Parser::parseBinOpExp(int minPrec)
{
	std::unique_ptr<Expression> lhs;
	//Parse the expression inside parentheses, skipping the parentheses themselves
	if (this->accept(TOKEN::OPEN_PAREN_TOKEN))
	{
		lhs = this->parseBinOpExp();
		this->expect(TOKEN::CLOSE_PAREN_TOKEN);
	}
	else
	{
		lhs = this->parsePrimaryExp();
	}

	if (!lhs)
	{
		std::cout << "No lhs\n";
		return nullptr;
	}
	// Create the Expression node for the AST
	auto exp = this->parseBinOpExpr(std::move(lhs), minPrec);

	return exp;
}

/*std::unique_ptr<Expression> Parser::parseDecl()
{

}*/

/*
* Parse an Expression, currently only handles arithmetic with parentheses
*/
std::unique_ptr<Expression> Parser::parseExp()
{
	auto exp = this->parseBinOpExp();
	return exp;
}

/*
* Evaluate the AST with the EvalVisitor and return a Value object with the result
*/
Value Parser::evalExp()
{
	std::unique_ptr<Expression> exp = this->parseExp();
	//There was a parsing error
	if (!exp)
	{
		std::stringstream err;
		err << "Parser::evalExp() : No Expression exception on line " << this->current.pos.first << ':' << this->current.pos.second;
		throw std::runtime_error(err.str().c_str());
	}

	EvalVisitor* vis = new EvalVisitor();

	//Try traversing the AST with the EvalVisitor
	try
	{
		exp->accept(vis);
	}
	catch (std::exception& e)
	{
		throw;
	}

	return vis->getValue();
}

/*
* Traverse the AST and print out each node
*/
void Parser::printExp()
{
	std::unique_ptr<Expression> exp = this->parseExp();
	if (!exp)
	{
		std::stringstream err;
		err << "Parser::evalExp() : No Expression exception on line " << this->current.pos.first << ':' << this->current.pos.second;
		throw std::runtime_error(err.str().c_str());
	}

	PrintVisitor* vis = new PrintVisitor();

	try
	{
		exp->accept(vis);
	}
	catch (std::exception& e)
	{
		throw;
	}
}