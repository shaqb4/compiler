#include "Visitor.h"
#include "AST.h"

#include <iostream>

/*
* Default constructor
*/
EvalVisitor::EvalVisitor()
{

}

/*
* Evaluate an binary aritmetic node
*/
void EvalVisitor::visit(ArithOp* op)
{
	if (op->getOper() == TOKEN::MODULO_TOKEN && (op->getChild(0)->getType() != DataType::INTEGER || op->getChild(1)->getType() != DataType::INTEGER))
	{
		throw std::logic_error("Modulo acccepts only integers\n");
	}

	//Left and right hand sides
	Value l, r;

	try
	{
		//Evaluate the lhs of the operator (child 0), sets the Value of this visitor
		op->getChild(0)->accept(this);
	}
	catch (std::exception& e)
	{
		std::cerr << "Could not accept left child\n";
		throw;
	}

	//Set l to the correct value and type depending on the value just evaluated
	if (this->val.Type == Value::DOUBLE)
		l.setValue(this->val.d);
	else
		l.setValue(this->val.i);

	//Repeat for the right hand side
	try
	{
		op->getChild(1)->accept(this);
	}
	catch (std::exception& e)
	{
		std::cerr << "Could not accept right child\n";
		throw;
	}
	if (this->val.Type == Value::DOUBLE)
		r.setValue(this->val.d);
	else
		r.setValue(this->val.i);

	//If either the lhs or rhs is a DOUBLE, ensure they are both doubles
	if (l.Type == Value::DOUBLE || r.Type == Value::DOUBLE)
	{
		if (l.Type == Value::INTEGER)
			l.d = l.i;
		if (r.Type == Value::INTEGER)
			r.d = r.i;

		l.Type = Value::DOUBLE;
		r.Type = Value::DOUBLE;
	}

	//Evaluate the operands with the correct operation
	switch (op->getOper())
	{
		case TOKEN::PLUS_TOKEN:
		{
			if (l.Type == Value::DOUBLE)
			{
				this->val.setValue(l.d + r.d);
			}
			else
			{
				this->val.setValue(l.i + r.i);
			}
			break;
		}
		case TOKEN::MINUS_TOKEN:
		{
			if (l.Type == Value::DOUBLE)
				this->val.setValue(l.d - r.d);
			else
				this->val.setValue(l.i - r.i);
			break;
		}
		case TOKEN::TIMES_TOKEN:
		{
			if (l.Type == Value::DOUBLE)
				this->val.setValue(l.d * r.d);
			else
				this->val.setValue(l.i * r.i);
			break;
		}
		case TOKEN::DIVIDE_TOKEN:
		{
			if (l.Type == Value::DOUBLE)
			{
				//Do not divide by 0
				if (r.d != 0)
				{
					this->val.setValue(l.d / r.d);
				}
				else
				{
					throw std::overflow_error("Divide by zero exception.");
				}
			}
			else
			{
				if (r.i != 0)
				{
					this->val.setValue(l.i / r.i);
				}
				else
				{
					throw std::overflow_error("Divide by zero exception.");
				}
			}
			break;
		}
		case TOKEN::MODULO_TOKEN:
		{
			//Modulo operation only applies to integers
			if (l.Type == Value::INTEGER && r.Type == Value::INTEGER)
			{
				this->val.setValue(l.i % r.i);
			}
			break;
		}
	}

}

/*
* Simply get the integer value of the node
*/
void EvalVisitor::visit(Integer* num)
{
	this->val.i = num->getValue();
	this->val.Type = Value::INTEGER;
}

/*
* Get the double value of the node
*/
void EvalVisitor::visit(Double* num)
{
	this->val.d = num->getValue();
	this->val.Type = Value::DOUBLE;
}

Value EvalVisitor::getValue()
{
	return this->val;
}



PrintVisitor::PrintVisitor()
{
	this->depth = 0;
}

void PrintVisitor::visit(ArithOp* op)
{
	for (int i = 0; i < depth * 2; i++)
	{
		std::cout << "-";
	}
	std::cout << Token::tokStrings[static_cast<int>(op->getOper())] << std::endl;
	depth++;
	op->getChild(0)->accept(this);
	op->getChild(1)->accept(this);
	depth--;
}

void PrintVisitor::visit(Double* op)
{
	for (int i = 0; i < depth * 2; i++)
	{
		std::cout << "-";
	}
	std::cout << op->getValue() << std::endl;
}

void PrintVisitor::visit(Integer* op)
{
	for (int i = 0; i < depth * 2; i++)
	{
		std::cout << "-";
	}
	std::cout << op->getValue() << std::endl;
}