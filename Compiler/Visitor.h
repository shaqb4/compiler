#ifndef VISITOR_H
#define VISITOR_H

//#include "AST.h"
#include "Value.h"
#include <memory>

class ArithOp;
class Integer;
class Double;

/*
* The base abstract Visitor class for traversing the AST
*/
class Visitor
{
public:
	virtual void visit(ArithOp* op) = 0;
	virtual void visit(Integer* num) = 0;
	virtual void visit(Double* num) = 0;
};

/*
* The EvalVistor does exactly what it sounds like. The compiler is more like a basic arithmetic interpreter at this point, implemented with this
* visitor
*/
class EvalVisitor : public Visitor
{
public:
	EvalVisitor();

	void visit(ArithOp* op);

	void visit(Integer* num);

	void visit(Double* num);

	Value getValue();

protected:
	Value val;
};

/*
* The PrintVistor prints the AST
*/
class PrintVisitor : public Visitor
{
public:
	PrintVisitor();

	void visit(ArithOp* op);

	void visit(Integer* num);

	void visit(Double* num);
protected:
	int depth;
};

#endif