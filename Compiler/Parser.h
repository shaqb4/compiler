#ifndef PARSER_H
#define PARSER_H

#include "Lexer.h"
#include "AST.h"

class Parser
{
protected:
	Lexer& scanner;
	std::unordered_map<string, Token> vars;
	std::unordered_map<string, int> precedence;
	Token current;

public:
	Parser(Lexer& lex);

	std::unique_ptr<Expression> parsePrimaryExp();

	std::unique_ptr<Expression> parseIntegerExp();

	std::unique_ptr<Expression> parseDoubleExp();
	
	/*std::unique_ptr<Expression> parseDecl();*/

	std::unique_ptr<Expression> parseBinOpExpr(std::unique_ptr<Expression> lhs, int minPrec);

	std::unique_ptr<Expression> parseBinOpExp(int minPrec = 0);

	std::unique_ptr<Expression> parseExp();

	Value evalExp();

	void printExp();

	Token getNextToken();

	bool accept(TOKEN tok);

	bool expect(TOKEN tok);

	bool currentIs(TOKEN tok);

	int getTokenPrecedence();
};

#endif