# Compiler Project #

This is a toy compiler I'm working on for fun, which I began after starting the Stanford Compilers Course on Coursera. 
My aim is to get a working, simple language that I can use and extend as I learn. So far, I have implemented a basic lexer and part of a parser.

## Current Status
The compiler currently doesn't generate any machine or bytecode to be executed. I am still implementing the lexer and parser to build up the 
Abstract Syntax Tree (AST) to represent the program. Right now, I use the visitor design pattern to traverse the AST and evaluate it directly.
It can evaluate relatively simple arithmetic expressions including \*,/,-,+ and % (modulo) and can handle parentheses correctly.

In the future I plan to use the LLVM compiler framework as a backend in order to generate machine codes for multiple platforms. Before then, I am working on designing
the language that I actually want to implement.
